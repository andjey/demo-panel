
// Initialize
// ---------------------------------------------------------------------

// Config can be extended here (before the any module is auto-loaded)
// Example: ` define([ 'config' ], function( config ) { config.extend = 'yes'; }); `



define( 'bootstrap', [ 'config', 'lib/dom' ], function( config, dom, load ) {

    // use config keys as module names
    var mod_names = Object.keys( config || {} ) || [];

    // when document ready
    dom().ready( function() {
        // include modules
        define( 'preload', mod_names, function() {

            // init each of them
            // console.log( 'preload:', arguments );
            var args = [].slice.call( arguments );
            args.forEach( function( mod, index ) {
                if ( mod && mod.init ) mod.init( config[ mod_names[ index ]] || {} );
                if ( !mod ) console.warn( 'no module', mod_names[ index ]);
            });

        });
    });
});

