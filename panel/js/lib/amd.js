/**
 * Hook AMD loader
 */


// AMD
( function() {
    var core = {};
    var modules = {};
    core.define = function( name, deps, fn ) {
        // optional args
        if ( arguments.length < 3 )
            fn = deps, deps = name, name = undefined;
        if ( arguments.length < 2 )
            fn = deps, deps = undefined;
        // real amd loader
        if ( 'function' == typeof define && define.amd )
            return define( name, deps, fn );
        else
        // pre-build modules
        var loaded = [];
        if ( deps && deps.length )
            for ( var i = 0; i < deps.length; i++ )
                loaded[i] = modules[ deps[ i ]];
        // local module registry
        if ( 'string' == typeof loaded ) loaded = [ loaded ];
        var mod = 'function' == typeof fn
            ? fn.apply( this, loaded || [] )
            : fn;
        if ( name ) modules[ name ] = mod;
    };
    if ( 'undefined' == typeof define )
        window.define = core.define;

    // auto-run script

})();

