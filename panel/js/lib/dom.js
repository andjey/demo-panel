/**
 * Simple DOM Interface
 *
 * Features:
 *  * Simple templating via data-binding (see: `populate()`)
 *
 */

define( 'lib/dom', [ 'lib/is' ], function( is ) {

    var
        // events
        ready_callbacks = [],
        event_w3c = !! document.addEventListener,
        event_ie = !! document.attachEvent,
        listeners = {},
        // custom events
        touch_moving = false;

    /**
     * Simple Dom Engine
     * @param {string|Element|Element[]|NodeList} [selector]
     * @param {Element} [parent]
     * @returns {*}
     * @constructor
     */
    function Dom( selector ) {
        if (!( this instanceof Dom ))
            return new Dom( selector );
        var list = elements( selector );
        this.length = list.length;
        for ( var i = 0; i < list.length; i ++ )
            this[ i ] = list[ i ];
    }

    var pro = Dom.prototype;


    // DOM
    // --------------------------------------------------------------------------------------------------------------

    function elements( selector ) {
        if ( selector instanceof Dom ) return selector;
        var
            // dom element
            el = selector && 'undefined' != typeof selector.nodeType ? selector: null,
            // html string
            html = 'string' == typeof selector && !!~selector.indexOf( '<' ) ? selector : null,
            // css selector
            query = !html && 'string' == typeof selector ? selector : null,
            // set of elements (or NodeList)
            list = is.Array( selector ) ? selector :
                is.NodeList( selector ) ? nodeListToArray( selector ) : null,
            stack, results = [];

        // prepare set of elements
        if ( el ) stack = [ el ];
        else if ( html ) stack = render( html );
        else if ( query ) stack = document.querySelectorAll( query );
        else if ( list ) stack = list;

        // filter elements
        if ( !stack || !stack.length ) return [];
        for ( var i = 0; i < stack.length; i ++ )
            // check is DOM element
            if ( stack[ i ] && 'undefined' != typeof stack[ i ].nodeType )
            results.push( stack[ i ]);
        return results;
    }

    /**
     * Find among children
     * @param {string} selector
     * @returns {Dom}
     */
    pro.find = function( selector ) {
        var list = [];
        for ( var i = 0; i < this.length; i ++ )
            list.push.apply( list, nodeListToArray(
                this[ i ].querySelectorAll && this[ i ].querySelectorAll( selector ) )
            );
        return new Dom( list );
    };

    /**
     * Get Children of each element in the set
     * @returns {Dom}
     */
    pro.children = function() {
        var list = [];
        for ( var i = 0; i < this.length; i ++ )
            list.push.apply( list, nodeListToArray( this[ i ].childNodes ));
        return new Dom( list );
    };

    /**
     * @returns {Element|undefined}
     */
    pro.first = function() {
        for ( var i = 0; i < this.length; i ++ )
            if ( this[ i ] && 1 === this[ i ].nodeType )
                return this[ i ];
    };


    // Class Names
    // --------------------------------------------------------------------------------------------------------------

    /**
     * Add class to the DOM element
     * @param {string[]|string} classes
     */
    pro.addClass = function( classes ) {
        var current, class_name;
        if ( !is.Array( classes )) classes = [ classes ];
        // each of elements in the set
        for ( var i = 0; i < this.length; i ++ ) {
            if ( !this[ i ].getAttribute ) continue;
            class_name = this[ i ].getAttribute( 'class' );
            current = class_name ? class_name.split( ' ' ) : [];
            // each of given class names
            for( var n = 0; n < classes.length; n ++ )
                // if class_name not found then push it to the set
                if ( !~ current.indexOf( classes[ n ]))
                    current.push( classes[ n ]);
            // update classes of element
            this[ i ].setAttribute( 'class', current.join( ' ' ));
        }
        return this;
    };

    /**
     * Remove class from DOM element
     * @param {string[]|string} classes
     */
    pro.removeClass = function( classes ) {
        var current, class_name, index;
        if ( !is.Array( classes )) classes = [ classes ];
        // each of elements in the set
        for ( var i = 0; i < this.length; i ++ ) {
            if ( !this[ i ].getAttribute ) continue;
            class_name = this[ i ].getAttribute( 'class' );
            current = class_name ? class_name.split( ' ' ) : [];
            // each of given class names
            for( var n = 0; n < classes.length; n ++  ) {
                index = current.indexOf( classes[ n ]);
                // if class_name not found then push it to the set
                if ( !~ index ) continue;
                // remove class_name from element list
                current.splice( index, 1 );
            }
            // update classes of element
            this[ i ].setAttribute( 'class', current.join( ' ' ));
        }
        return this;
    };


    // Update Properties
    // --------------------------------------------------------------------------------------------------------------

    // Attributes

    pro.attr = function( attrs ) {
        if ( arguments.length > 1 ) {
            var param = {};
            param[ arguments[ 0 ]] = arguments[ 1 ];
            attrs = param;
        }
        var names = Object.keys( attrs || {} ) || [];
        for ( var i = 0; i < this.length; i ++ )
            for ( var n = 0; n < names.length; n ++ )
                this[ i ] && this[ i ].setAttribute &&
                this[ i ].setAttribute( names[ n ], attrs[ names[ n ]] );
    };

    // CSS Styling

    pro.css = function( rules ) {
        var
            style_name,
            names = Object.keys( rules || {} ) || [];
        for ( var i = 0; i < this.length; i ++ )
            for ( var n = 0; n < names.length; n ++ ) {
                // camel-case name
                style_name = names[ n ].replace( /-(.)/g,
                    function( match, group ) {
                        return group.toUpperCase();
                    });
                // set element style property
                if ( this[ i ].style )
                    this[ i ].style[ names[ n ]] = rules[ names[ n ]];
            }
        return this;
    };

    // Text Node

    pro.text = function( text ) {
        this.empty();
        this.prepend( document.createTextNode( text ));
        return this;
    };

    // HTML Content

    pro.html = function( html ) {
        this.empty();
        this.append( elements( html ));
        return this;
    };


    // Manipulate
    // --------------------------------------------------------------------------------------------------------------

    /**
     * Append several child to DOM element
     * @param {Node|NodeList|[]} children
     */
    pro.append = function( children ) {
        var first = this.first();
        children = elements( children );
        if ( children )
            for ( var n = 0; n < children.length; n ++ )
                first && first.appendChild( children[ n ]);
        return this;
    };

    pro.prepend = function( children, after ) {
        var first = this.first();
        children = elements( children );
        if ( children )
            for ( var n = children.length; n > 0; n -- )
                first && first.insertBefore( children[ n - 1 ],
                    after || first.firstChild );
        return this;
    };

    pro.clone = function( deep ) {
        var list = [];
        if ( undefined === deep ) deep = true;
        for ( var i = 0; i < this.length; i ++ )
            list.push( this[ i ].cloneNode( !!deep ));
        return new Dom( list );
    };

    pro.empty = function() {
        for ( var i = 0; i < this.length; i ++ )
            while ( this[ i ].hasChildNodes() )
                this[ i ].removeChild( this[ i ].lastChild );
    };

    // Events
    // --------------------------------------------------------------------------------------------------------------

    function addEvent( element, type, listener, capture ) {
        if ( element.addEventListener ) element.addEventListener( type, listener, capture || false );  // w3c
        if ( element.attachEvent ) element.attachEvent( 'on' + type, listener );  // microsoft
        // custom only (not dom)
        return listener;
    }

    function removeEvent( element, type, listener, capture ) {
        if ( element.removeEventListener ) return element.removeEventListener( type, listener, capture || false ), true;  // w3c
        else if ( element.detachEvent ) return element.detachEvent( 'on' + type, listener );  // microsoft
        // custom only (not dom)
        return false;
    }

    // Listeners
    // ---------

    /**
     * Listen the event
     * @param {string} type
     * @param {function} handler
     * @param {boolean} [capture]
     * @returns {Dom}
     */
    pro.on = function( type, handler, capture, context ) {
        var bounded, element;
        // optional args
        if ( 'boolean' != typeof capture )
            context = capture, capture = undefined;
        // args
        handler = 'function' == typeof handler ? handler : false;
        // checking
        if ( !handler ) return null;

        // custom events polyfills

        // prevent click on scrolling
        if ( 'tap' == type && is.mobile ) {
            var _handler = handler;
            type = 'touchend';
            handler = function( e ) {
                if ( touch_moving ) return e.preventDefault(), false;
                _handler.apply( this, arguments );
            }
        }

        // prepare containers
        if ( !listeners[ type ]) listeners[ type ] = [];

        // attach event to each element from the set
        for ( var i = 0; i < this.length; i ++ ) {
            element = this[ i ];
            bounded = handler;
            if ( event_ie ) {
                // closure to access element from the event handler
                ( function( element ) {
                    // override handler
                    bounded = function( e ) {
                        var args = [].slice.call( arguments, 1 );
                        // event object
                        e = e || window.event || {};
                        if ( !e.target ) e.target = e.srcElement;
                        // call event handler
                        return handler.apply( context || element, [ e ].concat( args ));
                    };
                })( element );
            }
            // attach event
            addEvent( element, type, bounded, capture );
            // add to the set
            listeners[ type ].push({
                element: element,
                capture: capture,
                handler: handler,
                listener: bounded
            });
        }
        return this;
    };

    /**
     * Stop listen the event
     * Any arguments are optional.
     * You can miss any of the arguments to expand the set of events to remove.
     * @param {string|string[]} [type] - type of the event (or array of types)
     * @param {function} [handler] - original listener function
     * @param {boolean} [capture] - use capture (flag)
     * @returns {*}
     */
    pro.off = function( type, handler, capture ) {
        var
            v, i, n,
            self = this,
            types = ( type && [].concat( type ))
                || Object.keys( listeners );

        // remove all matching events
        types.forEach( function( t ) {
            for ( var i = 0; i < self.length; i ++ ) {
                if ( !listeners[ t ]) continue;
                // walk each of events of the given types
                for ( n = 0; n < listeners[ t ].length; n ++ ) {
                    v = listeners[ t ][ n ];
                    // remove matched events
                    if (( !self[ i ] || v.element === self[ i ])
                        && ( !handler || v.handler === handler )
                        && ( undefined === capture || v.capture === capture ))
                    {
                        // detach event
                        removeEvent( v.element, t, v.listener, v.capture );
                        // remove record from the set
                        // node: shift index, because of splice
                        listeners[ t ].splice( n --, 1 );
                    }
                }
            }
        });
        // clean empty
        types.forEach( function( t ) {
            if ( listeners[ t ] && !listeners[ t ].length )
                delete listeners[ t ];
        });
        return self;
    };


    // Custom events
    // --------------

    /**
     * Bind on_Document_Ready callback
     * @param {function} callback
     */
    pro.ready = function( callback ) {
        // checking
        if ( 'function' != typeof callback ) return;
        // already loaded
        if ( !!~ [ 'complete', 'loaded', 'interactive' ].indexOf( document.readyState ))
            return callback();
        // register main ready-event listener
        if ( !ready_callbacks.length )
            addEvent( document, event_ie ? 'readystatechange' : 'DOMContentLoaded', onReady );

        // keep callback function
        ready_callbacks.push( callback );
        // call each of ready-callbacks
        function onReady() {
            if ( event_ie && 'complete' != document.readyState ) return;
            removeEvent( event_ie ? 'readystatechange' : 'DOMContentLoaded', onReady );
            for ( var i = 0; i < ready_callbacks.length; i ++ ) ready_callbacks[ i ]();
            ready_callbacks = [];
        }
    };

    // Dimensions
    // --------------------------------------------------------------------------------------------------------------

    pro.width = function() {
        var first = this.first();
        return first && first.offsetWidth;
    };

    pro.height = function() {
        var first = this.first();
        return first && first.offsetHeight;
    };

    // Actions
    // --------------------------------------------------------------------------------------------------------------

    pro.hide = function() {
        this.attr({ hidden: true });
        this.css({ display: 'none' });
    };

    pro.show = function() {
        this.attr({ hidden: false });
        this.css({ display: 'block' });
    };

    // Template Engine
    // --------------------------------------------------------------------------------------------------------------

    /**
     * @params {Object} binding
     * @returns {Dom}
     * @example {
     *    // common syntax
     *    'li .node': {                         // css selector
     *        text: 'content',                  // text node string
     *        html: '<h1>overwrite</h1>',       // html string
     *        prepend: dom( 'h1' ),             // html|Dom|Element|NodeList
     *        append: dom( 'h1' ),              // html|Dom|Element|NodeList
     *        attr: { name: 'a1' },             // element attributes
     *        css: { backgroundColor: 'green }, // css properties
     *        class: [ 'active', 'next' ],      // string|array
     *        removeClass: 'passive'            // same as `[ 'passive' ]`
     *    },
     *    // simple text node
     *    '#test > div': 'text node',
     *    // reference to current element
     *    '<': { append: children }             // `<` - current element selector
     *  }
     */
    pro.populate = function( binding ) {
        var selector, data, el,
            names = Object.keys( binding || {} ) || [];
        // get each of binding
        for ( var n = 0; n < names.length; n ++ ) {
            selector = names[ n ];
            data = binding[ selector ];
            // text node syntax
            if ( 'string' == typeof data )
                this.find( selector ).prepend( document.createTextNode( data ));
            // common syntax
            if ( is.Object( data )) {
                el = '<' == selector ? this : this.find( selector );
                // insert text node
                if ( data.text ) el.text( data.text );
                // html content
                if ( data.html ) el.html( data.html );
                // append
                if ( data.append ) el.append( data.append );
                // prepend
                if ( data.prepend ) el.prepend( data.prepend );
                // update attributes
                if ( data.attr ) el.attr( data.attr );
                // apply css rules
                if ( data.css ) el.css( data.css );
                // change classes
                if ( data.class ) el.addClass( data.class );
                if ( data.removeClass ) el.removeClass( data.removeClass );
            }
        }
        return this;
    };

    // Helpers
    // --------------------------------------------------------------------------------------------------------------

    /**
     * Render html string to DOM elements
     * @param {string} html
     * @returns {Element[]}
     */
    function render( html ) {
        // create temporary container
        var el = document.createElement( 'div' );
        // render html-string to DOM elements
        el.insertAdjacentHTML( 'afterbegin', html );
        // convert NodeList to regular array
        return nodeListToArray( el.childNodes );
    }

    /**
     * Convert NodeList to the Array
     * @param {NodeList} nl
     * @returns {Array}
     */
    function nodeListToArray( nl ) {
        if ( !nl || !nl.length ) return [];
        for ( var list = [], i = 0, l = nl.length >> 0; i < l; list[ i ] = nl[ i ], i ++ );
        return list;
    }


    // Initialize
    // --------------------------------------------------------------------------------------------------------------

    // detect scrolling on mobile
    Dom( document )
        .off( 'touchmove' )
        .on( 'touchmove', function( e ) {
            touch_moving = true;
        })
        .off( 'touchend' )
        .on( 'touchend', function( e ) {
            touch_moving = false;
        });

    // API
    // --------------------------------------------------------------------------------------------------------------
    return Dom;

});