define( 'lib/is', function(){
    var
        is = {},
        div = document.createElement( 'div' ).style,
        ua = navigator.userAgent;

    // types
    is[ 'Object' ] = function( obj ) {
        return '[object Object]' == Object.prototype.toString.call( obj );
    };
    is[ 'Array' ] = function( obj ) {
        return '[object Array]' == Object.prototype.toString.call( obj );
    };
    // basic
    is[ 'String' ] = function( obj ) {
        return 'string' == typeof obj;
    };
    is[ 'Function' ] = function( obj ) {
        return 'function' == typeof obj;
    };
    // dom
    is[ 'Element' ] = function( obj ) {
        return ( obj && obj.nodeType );
    };
    is[ 'HtmlElement' ] = function( obj ) {
        return /\[object HTML\w*Element\]/
            .test( Object.prototype.toString.call( obj ));
    };
    is[ 'NodeList' ] = function( obj ) {
        return '[object NodeList]' == Object.prototype.toString.call( obj );
    };

    // css
    is[ 'transition' ] = !1
        || '' === div.MozTransition
        || '' === div.WebkitTransition
        || '' === div.OTransition
        || '' === div.transition;
    is[ 'transform' ] = !1
        || '' === div.MozTransform
        || '' === div.WebkitTransform
        || '' === div.OTransform
        || '' === div.transform;

    // platform
    is[ 'android' ] = /Android/.test( ua );
    is[ 'android_ver' ] = is.android && is.android.substr( ua.indexOf( 'Android' ) + 8, 3 );
    is[ 'ios' ] = /(iPhone|iPod|iPad)/.test( ua );
    is[ 'ios_ver' ] = is.ios && ua.substr( ua.indexOf( 'OS ' ) + 3, 3 ).replace( '_', '.' );
    // TODO: detect versions
    is[ 'winphone' ] = /windows phone/i.test( ua );
    is[ 'blackberry' ] = /blackberry|\bbb\d+/i.test( ua ) || /rim\stablet/i.test( ua );
    is[ 'webos' ] = /(web|hpw)os/i.test( ua );
    is[ 'baba' ] = /bada/i.test( ua );
    is[ 'tizen' ] = /tizen/i.test( ua );
    is[ 'firefoxos' ] = /firefox|iceweasel/i.test( ua ) && /\((mobile|tablet);[^\)]*rv:[\d\.]+\)/i.test( ua );
    // system
    is[ 'mobile' ] = is.android || is.ios || is.winphone || is.blackberry || is.webos || is.baba || is.tizen || is.firefoxos;
    is[ 'desktop' ] = !is.mobile;

    // API
    return is;
});