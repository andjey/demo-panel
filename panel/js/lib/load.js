/**
 * Loader
 */

define( 'lib/load', [], function() {

    /**
     * Cross browser XHR creation
     * @returns {XMLHttpRequest|ActiveXObject} - xhr
     */
    function request() {
        var req;
        // most of browsers
        if ( window.XMLHttpRequest ) req = new XMLHttpRequest();
        // IE implementation
        else if ( window.ActiveXObject ) {
            // different IE versions
            try { req = new ActiveXObject( 'Msxml2.XMLHTTP' ); }
            catch ( err ){}
            try { req = new ActiveXObject( 'Microsoft.XMLHTTP' ); }
            catch ( err ){}
        }
        return req;
    }

    /**
     * Ajax Query
     * Complete interface for AJAX queries
     * @param {object} params
     * @returns {Error|XMLHttpRequest|ActiveXObject} - error or xhr object
     */
    function ajax( params ) {
        params = params || {};
        var
        // params
            url = params.url,
            asyncMode = params.sync ? false : true,     // default: true
            method = params.method && params.method.toUpperCase() || 'GET',
            timeout = params.timeout || 30000,
            ontimeout = 'function' == typeof params.ontimeout ? params.ontimeout : false,
            cache = undefined !== params.cache ? !!params.cache : true,
            callback = 'function' == typeof params.callback ? params.callback : false,
            data = params.data instanceof  Object
                ? Object.keys( params.data ).map( function( k ) { return k + '=' + params.data[ k ] }).join( '&' )
                : params.data || '',
        // locals
            xhr = request();

        if ( !url ) return new Error( "Bad URL" );
        if ( !~ [ 'POST', 'GET', 'DELETE', 'PUT' ].indexOf( method ))
            return new Error( "Bad HTTP method" );
        if ( !xhr )  throw new Error( "Browser didn't support AJAX" );

        // Setup
        // -----

        // timeout
        if ( asyncMode ) {
            if ( timeout ) xhr.timeout = timeout;
            if ( ontimeout ) xhr.ontimeout = ontimeout;
        }
        // local urls
        if ( 'file:' == window.location.protocol )
            cache = true;
        // cache responses
        if ( !cache )
        // generate random id and append to the url
            url += ( !!~ url.indexOf( '?' ) ? '?' : '&' )
                + 'random=' + Math.random().toString( 16 ).substr( 2 );

        // request
        xhr.open( method, url, asyncMode );
        // post request need special headers
        if ( 'post' == params.method )
            xhr.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );

        // Action
        // ------

        // results
        function results() {
            if ( this.readyState != 4 ) return;
            var res = {
                code: this.status,
                status: this.statusText,
                response: this.responseText || this.responseXML || ''
            };
            // results
            if ( callback ) callback(
                res.code == 200 || res.code === 0 ? null : new Error( 'Ajax Error' ),
                res
            );
        }
        // async results handler
        if ( asyncMode ) xhr.onreadystatechange = results;
        // make query
        xhr.send( data );
        // sync results
        if ( !asyncMode ) results.apply( xhr );
        return xhr;
    }

    // Loader

    function css( url, callback ) {
        ajax({
            url: url,
            callback: function( err, res ) {

                // create stylesheet
                var style = document.createElement( 'style' );
                style.type = 'text/css';
                style.media = 'screen';
                // add css rules
                if ( style.styleSheet ) style.styleSheet.cssText = res.response;
                else style.appendChild( document.createTextNode( res.response ));
                // append to the header
                document.getElementsByTagName( 'head' )[ 0 ].appendChild( style );
                // callback
                callback && callback( null, style );
            }
        });
    }

    function json( url, callback ) {
        ajax({
            url: url,
            callback: function( err, res ) {
                var raw, json;
                // transport errors
                if ( err || !res || !res.response )
                    return callback( err || 'Empty JSON' );
                // parsing
                try {
                    // remove comments and parse
                    raw = res.response;
                    raw = raw.replace( /\/\*(.|[\r\n])*?\*\//gm, '' );
                    raw = raw.replace( /\/\/(.)*?/g, '' );
                    console.log( 'json:', raw );
                    json = JSON.parse( raw );
                }
                catch ( err ) {
                    return callback( 'Error parse JSON' );
                }
                callback && callback( null, json );
            }
        });
    }

    // API
    return {
        ajax: ajax,
        css: css,
        json: json
    };

});