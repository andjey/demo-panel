/**
 * Event Emitter
 */

define( 'lib/emitter', [], function() {

    // helpers
    var typeOf = Object.prototype.toString;

    /**
     * EventEmitter
     * @constructor
     */
    function EventEmitter() {
        // properties
        this._listeners = {};
        this._is_listened = false;
    }

    var pro = EventEmitter.prototype;

    /**
     * Listeners storage
     * @type {object} - { type: [ listeners ] }
     * @returns {EventEmitter}
     */
    pro._listeners = null;
    pro._is_listened = null;

    /**
     * Listen event
     * @param type
     * @param handler
     * @returns {EventEmitter}
     */
    pro.on = function( type, handler ) {
        var
            handler = 'function' == typeof handler ? handler : false,
            list = this._listeners[ type ] || [];
        // checking
        if ( !handler ) return this;
        if ( !!~list.indexOf( handler )) return this;
        // register listener
        list.push( handler );
        this._listeners[ type ] = list;
        // set flag
        this._is_listened = true;
        // chain
        return this;
    };

    /**
     * Stop listening event
     * @param {string|string[]} type
     * @param {function} handler
     * @returns {EventEmitter}
     */
    pro.off = function( type, handler ) {

        // array of types
        var self = this;
        if ( '[object Array]' == typeOf.call( type ))
            return type.forEach( function( v ) {
                self.off( v, handler );
            }), this;
        var
            list = this._listeners[ type ] || [],
            index = list.indexOf( handler );

        // args: remove all listener
        if ( !handler ) list = [];
        // args: remove given handler only
        else if ( !!~index ) {
            list.splice( index, 1 );
            // clean if need
            if ( !list.length )
                delete this._listeners[ type ];
        }
        this._listeners = list;

        // reset flags
        if ( !Object.keys( this._listeners ).length )
            this._is_listened = false;

        // chain
        return this;
    };

    /**
     * Cast event
     * ( call event listener with given arguments )
     * @param {string} type
     * @param {...*} [arg]
     * @returns {EventEmitter}
     */
    pro.emit = function( type, arg ) {
        var
            args = [].slice.call( arguments ).slice( 1 ),
            list = this._listeners[ type ] || [];

        // call each of listeners
        list.forEach( function( fn ) {
            fn.apply( null, args );
        });
        // successful
        return this;
    };

    pro.emitAll = function( type, arg ) {
        // first, emit the event
        var args = arguments;
        this.emit.apply( this, args );
        // next, dispatch event to each of parents
        if ( this.parents && this.parents.length )
            this.parents.forEach( function( parent ) {
                parent.emitAll.apply( parent, args );
            });
        // then, success
        return this;
    };

    /**
     * Attach EventEmitter to any other object
     * @param {*} instance
     * @returns {EventEmitter}
     */
    pro.attach = function( instance ) {
        if ( !instance ) return this;
        for ( var key in proto )
            if ( proto.hasOwnProperty( key ))
                instance[ key ] = proto[ key ];
        return this;
    };

    return EventEmitter;

});