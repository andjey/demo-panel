define( 'play/suspend', [ 'panel/panel' ], function( panel ) {
    var
        on_pause,
        on_resume;

    function init( config ) {

        // configure
        config = config && config.game || {};
        on_pause = 'function' == typeof config.pause ? config.pause : false;
        on_resume = 'function' == typeof config.resume ? config.resume : false;

        // initialize
        panel.events.on( 'open', pause );
        panel.events.on( 'close', resume );
    }

    function pause() {
        console.log( 'pause' );
        // pause game
        if ( 'undefined' != typeof meshPause ) meshPause();
        // callback
        on_pause && on_pause();
    }

    function resume() {
        console.log( 'resume' );
        // resume game
        if ( 'undefined' != typeof meshResume ) meshResume();
        // callback
        on_resume && on_resume();
    }

    // API
    return {
        init: init,
        // actions
        pause: pause,
        resume: resume
    };
});