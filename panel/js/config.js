define( 'config', [], {

    // Configuration

    // Panel
    // ---------------------------------------------------------------------

    'panel/panel': {
        parent: '.slider',              // css selector: page container
        open: '.panel-icon',            // css selector: panel opening button
        close: '.close-panel-button',   // css selector: close button
        overlay: '.overlay',            // css selector: overlay
        position: 'left',               // left, right
        icon: {                         // icon to open slider
            opacity: .75                // 1 = fully visible
        }
    },
    'panel/link': {                     // main link
        label: 'More Games',            // text of
        url: 'http://m.softgames.de',   // url of
        callback:                       // on_click callback `function() || null`
            function() {
                console.log( 'more games' );
            }
    },
    'panel/share': {
        url: 'http://softgames.de',
        title: 'title',
        message: 'message',
        networks: {
            facebook: {
                callback:               // on_click callback `function() || null`
                    function() {
                        alert( 'facebook' );
                    }
            },
            twitter: {
                callback:               // on_click callback `function() || null`
                    function() {
                        alert( 'twitter' );
                    }
            }
        }
    },
    'panel/stars': {
        amount: 5,                      // amount of stars in row
        rating: 0,                      // current rating (amount of active stars)
        callback:                       // on_click callback `function( rating ) || null`
            function( rating ) {
                console.log( 'rating ' + rating );
            }
    },
    'panel/links': [                    // list of additional links
        {
             label: 'Link 1',
             callback:
                 function() {
                     console.log( 'link 1' );
                 }
        },
        {
            label: 'Another Link',
            callback:
                function() {
                    console.log( 'link 2' );
                }
        }
    ],

    // Game play
    // ---------------------------------------------------------------------

    'play/suspend': {
        // TODO
        pause: null,                    // function to pause the game
        resume: null,                   // function to resume the game
        on_pause: null,                 // callback on game paused
        on_resume: null                 // callback on game resumed
    }

});