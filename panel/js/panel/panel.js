/**
 * Slide-over menu panel
 *
 * @typedef {String|Element|NodeList|Dom} Html
 */

define( 'panel/panel', [
    'lib/is',
    'lib/dom',
    'lib/emitter',
    'lib/load',
    'text!view/panel.html',
    'text!view/panel-group.html',
    'text!view/panel-item.html',
    'text!view/panel-overlay.html'
],
function(
    // libs
    is,
    dom,
    EventEmitter,
    load,
    // templates
    html_panel,
    html_group,
    html_block,
    html_overlay
){

    // Configure
    // ---------------------------------------------------------------------

    var
        // locals
        groups = {},
        // views
        el_body = dom( 'body' ),
        el_panel = dom( html_panel ),
        el_block = dom( html_block ),
        el_group = dom( html_group ),
        el_overlay = dom( html_overlay ),
        // links
        el_page,
        el_open,
        el_close,
        container = el_panel.find( '.panel-container > div' ),
        // events
        moves_now = false,
        click_event = is.desktop ? 'click' : 'tap',
        events = new EventEmitter();


    // Initialize
    // ---------------------------------------------------------------------

    function init( cfg ) {
        // console.info( 'init panel', cfg );
        cfg = cfg || {};

        // platform issues
        if ( is.android && is.android_ver < 3 || is.ios && is.ios_ver < 5 )
            dom( 'html' ).addClass( 'scroll-naturally' );

        // load css styles
        load.css( 'css/panel.css' );

        // root element
        el_page = dom( cfg.parent || 'body' );

        // overlay element
        el_page.append( el_overlay );

        // panel element
        // panel position
        el_panel.addClass( 'to-' + ( cfg.position || 'left' ));
        // apply transition
        el_page.addClass( 'transition' );
        el_panel.addClass( 'transition' );
        // insert behind any element of body
        el_page.append( el_panel );

        // create groups
        // main group (!)
        group( 'main', 'Features' );
        // link list
        group( 'links', 'Links' );

        // opening icon
        el_open = dom( cfg.open );
        if ( cfg.icon ) {
            if ( !isNaN( parseFloat( cfg.icon.opacity )))
                el_open.css({ opacity: parseFloat( cfg.icon.opacity ) });
        }

        // events

        // open panel
        el_open.off().on( click_event, function() {
            // show panel
            el_page.addClass( 'panel-open' );
            // show overlay
            el_overlay.removeClass( 'hidden' );
            // cast event
            events.emit( 'open' );
        });

        // close panel
        el_close = dom( cfg.close );
        el_close.off().on( click_event, hidePanel );
        // force panel closing
        el_overlay.off().on( click_event, hidePanel );

        function hidePanel() {
            // hide panel
            el_page.removeClass( 'panel-open' );
            // hide overlay
            el_overlay.addClass( 'hidden' );
            // cast event
            events.emit( 'close' );
        }
    }


    // Modifiers
    // ---------------------------------------------------------------------

    /**
     * Create group of items
     * @param {String} name
     * @param {String} title
     */
    function group( name, title ) {
        // group element
        var el = el_group.clone();
        title = '' + ( title || name || '' );
        el.populate({ 'header': title });
        container.append( el );
        // hide empty group
        el.hide( 'hide' );
        // save record
        groups[ name ] = {
            el: el,
            title: title,
            container: el.find( '.list ul' )
        };
    }

    /**
     * Add item to the panel
     * @param {{ click:Function, group:String, icon:String, label:String, inline:Html, content:Html }} params
     */
    function item( params ) {
        var el, container, group;
        // checking
        if ( !params || !is.Object( params )) return;
        if ( params.click && !is.Function( params.click )) return;
        // create item
        el = el_block.clone();
        if ( params.click ) {
            // attach click event
            el.find( '.click' ).off( 'click' ).on( click_event, params.click );
            // mark item as clickable
            el.find( '.stack' ).addClass( 'clickable' );
        }
        // bind data
        el.populate({
            // show icon
            '.glyph': params.icon && { class: [ 'icon', 'icon-' + params.icon ]},
            // text of item
            '.link': params.label && { text: '' + ( params.label || '' )},
            // append item inline content (like small buttons)
            '.stack': params.inline && { append: params.inline },
            // append content to the end of element
            '<': params.content && { append: params.content }
        });
        // append to the dom parent
        group = params.group ? groups[ params.group ] : groups.main;
        if ( !group ) return;
        // update group element
        group.container && group.container.append( el );
        group.el && group.el.show();
    }


    // Actions
    // ---------------------------------------------------------------------

    /**
     * Open panel
     */
    function open() {
        el_panel.addClass( 'sg-panel-show' );
        // el_panel.css({ left: 0 });
        events.emit( 'open' );
    }
    /**
     * Close panel
     */
    function close() {
        el_panel.removeClass( 'sg-panel-show' );
        // el_panel.style.left = - el_panel.width();
        events.emit( 'close' );
    }


    // API
    // ---------------------------------------------------------------------

    return {
        init: init,
        events: events,
        // modifiers
        group: group,
        item: item,
        // actions
        open: open,
        close: close
    };

});
