define( 'panel/link', [  'lib/dom', 'panel/panel' ], function( dom, panel ) {

    /**
     * Initialize
     * @param config
     */
    function init( config ) {
        // initialize
        // `setTimeout` because `alert()` blocks events
        if ( config ) create( config.label, function() {
            if ( config.callback ) setTimeout( config.callback, 0 );
        });
    }

    /**
     * Add Link to the Panel list
     * @param {String} label
     * @param {Function} callback
     */
    function create( label, callback ) {
        //var html = '<a class="sg-link" href="' + url + '">' + ( label || url ) + '</a>';
        //menu.add( dom( html ));
        panel.item({
            label: label,
            icon: 'more',
            click: callback
        });
    }

    // API
    return {
        init: init,
        create: create
    };

});