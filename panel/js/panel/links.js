define( 'panel/links', [ 'lib/dom', 'panel/panel' ], function( dom, panel ) {

    /**
     * Initialize
     * @param {{}} config
     */
    function init( config ) {
        // initialize
        if ( config && config.length )
            config.forEach( function( link ) {
                // `setTimeout` because `alert()` blocks events
                if ( config ) create( link.label, function() {
                    if ( link.callback ) setTimeout( link.callback, 0 );
                });
            });
    }

    /**
     * Add Link to the Panel list
     * @param {string} label
     * @param {Function} callback
     */
    function create( label, callback ) {
        panel.item({
            group: 'links',
            label: label,
            click: callback
        });
    }

    // API
    return {
        init: init,
        create: create
    };

});