define( 'panel/share', [
    'lib/dom',
    'lib/is',
    'panel/panel',
    'text!view/share.html',
],
function(
    // libs
    dom,
    is,
    // components
    panel,
    // templates
    html_share
){
    var event_name = is.desktop ? 'click' : 'tap';  // `tap` is custom click event on mobile

    function init( config ) {
        var
            // elements
            el = dom( html_share ),
            // share data
            url = config.url || window.location.href,
            title = config.title || document.title,
            message = config.message || '',
            // networks
            names = Object.keys( config.networks || {} ) || [];

        // element
        // create sharing item
        panel.item({
            label: 'Share',
            icon: 'share',
            inline: el
        });
        // networks
        names.forEach( function( name ) {
            // events
            el.find( '.share-' + name ).off().on( event_name,
                function() {
                    // `setTimeout` because `alert()` blocks events
                    setTimeout( config.networks[ name ].callback, 0 );
                });
        });
    }

    return {
        init: init
    };
});