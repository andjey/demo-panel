define( 'panel/stars', [
    'lib/is',
    'lib/dom',
    'lib/emitter',
    'panel/panel',
    'text!view/stars.html',
    'text!view/stars-item.html'
], function(
    // libs
    is,
    dom,
    EventEmitter,
    // component
    panel,
    // templates
    html_stars,
    html_item
){
    var
        // settings
        amount,
        rate,
        // class names
        active_class = 'active',
        // elements
        stars,
        star_elements = [],
        // events
        events = new EventEmitter();

    function init( config ) {
        // settings
        amount = config.amount >> 0 || 5;
        rate = config.rate >> 0 || 0;
        var
            callback = 'function' == typeof config.callback ? config.callback : false,
            event_name = is.desktop ? 'click' : 'tap';  // `tap` is custom click event on mobile

        // compose stars
        stars = dom( html_stars );

        for ( var i = 0; i < amount; i ++ ) {
            // create item
            var child = dom( html_item );
            child.attr( 'data-star', '' + ( i + 1 ));
            stars.append( child );
            star_elements.push( child );

            // on click star
            child.on( event_name, function( e ) {
                var index = this.getAttribute( 'data-star' );
                // reactivate stars
                activate( index );
                // cast event
                events.emit( 'star', index );
                // callback on click
                // `setTimeout` because `alert()` blocks events
                if ( callback ) setTimeout( function() {
                    callback( index );
                }, 0 );
                // stop event flow
                e && e.preventDefault && e.preventDefault();
                return false;
            });
        }

        // default amount of stars
        activate( config.rating );
        // debug: add to the panel
        //panel.add( stars );
        panel.item({
            label: 'Rate the Game',
            icon: 'rate',
            content: stars
        });
    }


    function activate( rating ) {
        rate = rating >> 0;
        if ( rate < 0 ) rate = 0;  // min
        if ( rate > amount ) rate = amount;  // max
        // walk stars
        for ( var i = 0; i < amount; i ++ ) {
            if ( !star_elements[ i ]) break;
            // activate star
            if ( i < rate ) star_elements[ i ].addClass( active_class );
            // deactivate star
            else star_elements[ i ].removeClass( active_class );
        }
    }

    return {
        init: init,
        events: events,
        // actions
        activate: activate
    };

});