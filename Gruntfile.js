module.exports = function( grunt ) {

    grunt.loadNpmTasks( 'grunt-svg-sprite' );
    grunt.loadNpmTasks( 'grunt-webfont' );
    grunt.loadNpmTasks( 'grunt-rename' );
    grunt.loadNpmTasks( 'grunt-contrib-copy' );
    grunt.loadNpmTasks( 'grunt-contrib-less' );
    grunt.loadNpmTasks( 'grunt-contrib-requirejs' );
    grunt.loadNpmTasks( 'grunt-contrib-uglify' );

    grunt.initConfig({

        // https://github.com/jkphl/grunt-svg-sprite
        svgsprite: {
            options: {
                // Task-specific options go here.
            },
            your_target: {
                src      : 'panel/img/icons',
                dest     : '',
                options  : {
                    spritedir: '/panel/img/',
                    sprite: 'icons',
                    prefix: 'icon',
                    layout: 'vertical',
                    render    : {
                        css     : false,
                        less    : {
                            dest  : 'panel/less/sprites'
                        }
                    }
                }
            }
        },

        // https://github.com/sapegin/grunt-webfont
        webfont: {
            icons: {
                src: 'panel/img/icons/*.svg',
                dest: 'panel/font/icons',
                destCss: 'panel/less/theme',
                options: {
                    font: 'icons',
                    relativeFontPath: 'font/icons',
                    stylesheet: 'less',
                    htmlDemo: false,
                    templateOptions: {
                        classPrefix: 'icon-',
                        mixinPrefix: 'mix-icon-'
                    }
                }
            }
        },

        // https://github.com/gruntjs/grunt-contrib-less
        less: {
            development: {
                options: {
                    cleancss: true  // minify
                },
                files: {
                    "panel/css/panel.css": "panel/less/panel.less"
                }
            }
        },

        // https://github.com/gruntjs/grunt-contrib-requirejs
        requirejs: {
            compile: {
                options: {
                    baseUrl: 'panel/js',
                    paths: {
                        view: '../view',
                        text: 'vendor/requirejs-text/text'
                    },
                    inlineText:  true,
                    wrap: true,
                    out: 'public/libs/panel.min.js',
                    optimize: 'uglify',
                    // optimize: 'none',
                    preserveLicenseComments: false,

                    stubModules: [ 'text' ],

                    include: [
                        // hook amd loader
                        'lib/amd.js',
                        // each available modules
                        'panel/fullscreen',
                        'panel/link',
                        'panel/links',
                        'panel/panel',
                        'panel/share',
                        'panel/stars',
                        'play/suspend',
                        // application bootstrap
                        'config',
                        'index.js'
                    ]
                }
            }
        },

        // https://github.com/gruntjs/grunt-contrib-copy
        copy: {
            fonts: {
                expand : true,
                dest   : 'public/font',
                cwd    : 'panel/font',
                src    : [ '**/*' ]
            },
            css: {
                expand : true,
                dest   : 'public/css',
                cwd    : 'panel/css',
                src    : [ '**/*' ]
            }
        }


        });


    // Tasks
    // -----

    grunt.registerTask( 'default', [ 'webfont', 'less', 'requirejs', 'copy' ]);


};