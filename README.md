
Panel
=====


Features
--------

 * Common configuration (`addon/js/config.js`)
 * Totally without dependencies just native JavaScript
 * All modules made from scratch
 * Decoupled structure (with AMD)
 * Nothing is stored in the global scope (except `define`)

 * Simple DOM interface (elements/css) with eventing and templating
 * Basic Browser detection (see `addon/js/is.js`, which can be extended)
 * Event Emitter makes event-driven inner interface
 
 * Prevent click-events when panel scroll
 * Callbacks when panel opened


Usage
-----

First, change directory to the project path.

If you want to build from the sources, you must first install:
	
	# osx
	brew install ttfautohint fontforge --with-python
	# linux
	sudo apt-get install fontforge ttfautohint
	# win
	open http://www.freetype.org/ttfautohint/#download
	
	# then
	npm install
	
But if you only want to run the demo, just execute: 

	$
	npm install --production
	npm start
	open http://localhost:8880
	
Enjoy. Thank you.


TODO
----

Some of good staff to do later:

* Progress bar of loading
* Complete module of sharing
* Testing for android and windows.phone devices (didn't have devices at hand)
* Create unit tests


Author
------

Andjey Guzhovskiy <me.the.ascii@gmail.com>
